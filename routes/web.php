<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('api/register',['uses'=>'LoginController@register']);
$router->post('api/login',['uses'=>'LoginController@login']);

$router->group(['prefix'=>'api','middleware'=>'auth'], function() use ($router){

    $router->get('/data',['uses'=>'DataController@index']);
    $router->get('/data/{id}',['uses'=>'DataController@show']);
    $router->post('/data',['uses'=>'DataController@create']);
    $router->delete('/data/{id}',['uses'=>'DataController@destroy']);
    $router->put('/data/{id}',['uses'=>'DataController@update']);
});     